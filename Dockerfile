FROM node:14-alpine as api
# Add a work directory
WORKDIR /api
# Cache and Install dependencies
COPY package.json .
COPY package-lock.json .
COPY ormconfig.json .
RUN npm install
RUN npm install -g nodemon
COPY . .
EXPOSE 5000
CMD [ "nodemon", "src/server.ts" ]
