import moment from 'moment';
import { Formats } from '../constants/formats';
import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, BeforeInsert, BeforeUpdate } from 'typeorm';
import { Users } from './Users';

@Entity()
export class Orders {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne((type) => Users, (users) => users.id, { onDelete: 'CASCADE', eager: true })
    @JoinColumn()
    public users: Users;

    @Column()
    public name: string;

    @Column()
    public orderedDatetime: string;

    @Column()
    public paymentOption: number;

    @Column()
    public status: number;

    @Column()
    public createdAt: string;

    @Column()
    public updatedAt: string;

    @BeforeInsert()
    updateDateCreation() {
        this.createdAt = moment().format(Formats.MYSQL_DATE_TIME);
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
        this.orderedDatetime = moment().format(Formats.MYSQL_DATE_TIME);
        this.status = 0;
    }

    @BeforeUpdate()
    updateDateUpdate() {
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }
}