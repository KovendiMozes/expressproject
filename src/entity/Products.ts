import moment from 'moment';
import { Formats } from '../constants/formats';
import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, BeforeInsert, BeforeUpdate } from 'typeorm';
import { Shops } from './Shops';

@Entity()
export class Products {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne((type) => Shops, (shops) => shops.id, { onDelete: 'CASCADE', eager: true })
    @JoinColumn()
    public shops: Shops;

    @Column()
    public name: string;

    @Column()
    public description: string;

    @Column("decimal", { precision: 11, scale: 2 })
    public price: number;

    @Column()
    public createdAt: string;

    @Column()
    public updatedAt: string;

    @BeforeInsert()
    updateDateCreation() {
        this.createdAt = moment().format(Formats.MYSQL_DATE_TIME);
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }

    @BeforeUpdate()
    updateDateUpdate() {
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }
}