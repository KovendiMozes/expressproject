import moment from 'moment';
import { Formats } from '../constants/formats';
import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne, BeforeInsert, BeforeUpdate } from 'typeorm';
import { Users } from './Users';

@Entity()
export class BankCards {
    @PrimaryGeneratedColumn()
    public id: number;

    @OneToOne((type) => Users, (users) => users.id, { onDelete: 'CASCADE', eager: true })
    @JoinColumn()
    public users: Users;

    @Column()
    public cardNumber: string;

    @Column()
    public cardholderName: string;

    @Column()
    public date: string;

    @Column()
    public CVC: number;

    @Column()
    public createdAt: string;

    @Column()
    public updatedAt: string;

    @BeforeInsert()
    updateDateCreation() {
        this.createdAt = moment().format(Formats.MYSQL_DATE_TIME);
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }

    @BeforeUpdate()
    updateDateUpdate() {
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }
}