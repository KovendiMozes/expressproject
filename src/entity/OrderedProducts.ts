import moment from 'moment';
import { Formats } from '../constants/formats';
import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, BeforeInsert, BeforeUpdate } from 'typeorm';
import { Orders } from './Orders';
import { Products } from './Products';

@Entity()
export class OrderedProducts {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne((type) => Orders, (orders) => orders.id, { onDelete: 'CASCADE', eager: true })
    @JoinColumn()
    public orders: number;

    @ManyToOne((type) => Products, (products) => products.id, { onDelete: 'CASCADE', eager: true })
    @JoinColumn()
    public products: number;

    @Column()
    public createdAt: string;

    @Column()
    public updatedAt: string;

    @BeforeInsert()
    updateDateCreation() {
        this.createdAt = moment().format(Formats.MYSQL_DATE_TIME);
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }

    @BeforeUpdate()
    updateDateUpdate() {
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }
}