import moment from 'moment';
import { Formats } from '../constants/formats';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, BeforeInsert, BeforeUpdate } from 'typeorm';
import { Shops } from './Shops';

@Entity()
export class Users {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne((type) => Shops, (shops) => shops.id, { nullable: true , eager: true })
    @JoinColumn()
    public shops: Shops;

    @Column()
    public fullname: string;

    @Column()
    public username: string;

    @Column()
    public email: string;

    @Column()
    public password: string;

    @Column()
    public telephone: string;

    @Column()
    public address: string;

    @Column()
    public isCourier: number;

    @Column()
    public createdAt: string;

    @Column()
    public updatedAt: string;

    @Column()
    public accessToken: string;

    @Column()
    public refreshToken: string;

    @BeforeInsert()
    updateDateCreation() {
        this.createdAt = moment().format(Formats.MYSQL_DATE_TIME);
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }

    @BeforeUpdate()
    updateDateUpdate() {
        this.updatedAt = moment().format(Formats.MYSQL_DATE_TIME);
    }
}