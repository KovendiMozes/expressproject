import { Products } from '../entity/Products';
import { EntityRepository, Repository, getCustomRepository, MoreThan, Like } from 'typeorm';

@EntityRepository(Products)
class ProductRepository extends Repository<Products>{
    /**
     * Find products by shop id
     * @param shopId
     */
    public findAll = async (shopId: number): Promise<Products[]> => {
        return await this.find({
            where: {shops: shopId}
        });
    }

    /**
     * Find product by id
     * @param id
     */
    public findById = async (id: number): Promise<Products> => {
        const product: Products[] = await this.find({
           where: {id: id}
        });
        return product[0];
    }

    /**
     * Find product by name and shop id
     * @param name
     * @param shopId
     */
    public findByNameAndShopId = async (name: string, shopId: number): Promise<Products[]> => {
        return  await this.find({
            where: {name: Like('%'+name+'%'), shops: shopId}
        });
    }

    /**
     * Find that product name is unique this shop
     * @param name
     * @param shopId
     */
    public findThatProductNameIsUniqueThisShop = async (name: string, shopId: number): Promise<Products> => {
        const product: Products[] = await this.find({
            where: {name: name, shops: shopId}
        });
        return product[0];
    }
}

export default getCustomRepository(ProductRepository);