import { Orders } from '../entity/Orders';
import { EntityRepository, Repository, getCustomRepository, MoreThan, Like } from 'typeorm';

@EntityRepository(Orders)
class OrderRepository extends Repository<Orders>{
    /**
     * Find all active orders
     */
    public findAll = async (): Promise<Orders[]> => {
        return await this.find({
            where: {status: 0}
        });
    }

    /**
     * Find order by id
     * @param id
     */
    public findById = async (id: number): Promise<Orders> => {
        const order: Orders[] = await this.find({
            where: {id: id}
        });
        return order[0];
    }

    /**
     * Find user all own orders
     * @param id
     */
    public findOrdersByUserId = async (id: number): Promise<Orders[]> => {
        return await this.find({
            where: {users: id}
        });
    }

    /**
     * Find user all own orders by datetime
     * @param id
     * @param datetime
     */
    public findOrdersByUserIdAndDatetime = async (id: number, datetime: string): Promise<Orders[]> => {
        return await this.find({
            where: {users: id, orderedDatetime: Like(datetime)}
        });
    }

    /**
     * Find orders by datetime
     * @param datetime
     */
    public findOrdersByDatetime = async (datetime: string): Promise<Orders[]> => {
        return await this.find({
            where: {orderedDatetime: Like(datetime), status: 0}
        });
    }
}

export default getCustomRepository(OrderRepository);