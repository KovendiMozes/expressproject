import {Users} from "../entity/Users";
import { EntityRepository, Repository, getCustomRepository, MoreThan } from 'typeorm';

@EntityRepository(Users)
class UserRepository extends Repository<Users>{
    public findAll = async (): Promise<Users[]> => {
        return await this.find({
        });
    };

    public findById = async (id: number): Promise<Users> => {
        const user: Users[] = await this.find({
            where: {id: id}
        });
        return user[0];
    }

    public findCouriers = async (): Promise<Users[]> => {
        return await this.find({
            where: {is_courier: 1}
        });
    }

    public findByUsername = async (username: string): Promise<Users> => {
        const user: Users[] = await this.find({
            where: {username: username}
        });
        return user[0];
    }
}

export default getCustomRepository(UserRepository);