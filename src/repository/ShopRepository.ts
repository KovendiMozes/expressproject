import { EntityRepository, Repository, getCustomRepository, MoreThan } from 'typeorm';
import { Shops } from "../entity/Shops";

@EntityRepository(Shops)
class ShopRepository extends Repository<Shops> {
    /**
     * Find all shop
     */
    public findAll = async (): Promise<Shops[]> => {
        return await this.find({
        });
    };

    /**
     * Find shop by id
     * @param id
     */
    public findById = async (id: number): Promise<Shops> => {
        const shops: Shops[] = await this.find({
            where: {id: id}
        });
        return shops[0];
    }

    /**
     * Find shop by name
     * @param name
     */
    public findByName = async (name: string): Promise<Shops> => {
        const shops: Shops[] = await this.find({
           where: {name: name}
        });
        return shops[0];
    }
}

export default getCustomRepository(ShopRepository);