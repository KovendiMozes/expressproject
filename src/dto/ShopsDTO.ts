import { Formats } from '../constants/formats';
import moment from 'moment';
import { Shops } from '../entity/Shops';
import { ErrorDTO } from './ErrorDTO';
import { Response } from 'express';
import { HttpStatus } from '../constants/httpStatus';

export class ShopsDTO {
    public id: number;
    public name: string;
    public address: string;
    public telephone: string;
    public createdAt: string;
    public updatedAt: string;

    /**
     * From database
     * @param shops
     */
    public fromShops(shops: Shops): ShopsDTO {
        const shopsDTO: ShopsDTO = new ShopsDTO();
        shopsDTO.id = shops.id;
        shopsDTO.name = shops.name;
        shopsDTO.telephone = shops.telephone;
        shopsDTO.address = shops.address;
        shopsDTO.createdAt = shops.createdAt;
        shopsDTO.updatedAt = shops.updatedAt;

        return shopsDTO;
    }

    /**
     * Name validation
     * @param name
     * @private
     */
    private nameValidation(name: string): boolean{
        let regex: RegExp =  /^[\s\w]{3,}$/;
        if(name.match(regex)){
            return true;
        }
        return false;
    }

    /**
     * To database
     * @param shopsDTO
     * @param response
     */
    public toShops(shopsDTO: ShopsDTO, response: Response): Shops | ErrorDTO[]{
        const errorDTOs: ErrorDTO[] = [];
        if(!shopsDTO.name){
            errorDTOs.push(new ErrorDTO().toError('The name is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!shopsDTO.telephone){
            errorDTOs.push(new ErrorDTO().toError('The telephone is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!shopsDTO.address){
            errorDTOs.push(new ErrorDTO().toError('The address is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!this.nameValidation(shopsDTO.name)){
            errorDTOs.push(new ErrorDTO().toError('Incorrect shop name.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(shopsDTO.telephone.replace(/\s+/g, '').length !== 10){
            errorDTOs.push(new ErrorDTO().toError('Incorrect telephone number.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(errorDTOs.length){
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }
        const shops: Shops = new Shops();
        shops.id = shopsDTO.id;
        shops.name = shopsDTO.name;
        shops.telephone = shopsDTO.telephone;
        shops.address = shopsDTO.address;
        shops.createdAt = shopsDTO.createdAt;
        shops.updatedAt = shopsDTO.updatedAt;

        return shops;
    }
}

