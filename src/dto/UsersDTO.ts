import { Users } from '../entity/Users';
import { Formats } from '../constants/formats';
import moment from 'moment';
import { ShopsDTO } from '../dto/ShopsDTO';
import { Shops } from '../entity/Shops';
import { ErrorDTO } from './ErrorDTO';
import { Response } from 'express';
import { HttpStatus } from '../constants/httpStatus';

export class UsersDTO{
    public id: number;
    public shop: ShopsDTO;
    public fullname: string;
    public username: string;
    public email: string;
    public password: string;
    public telephone!: string;
    public address: string;
    public isCourier: number;
    public accessToken: string;
    public refreshToken: string;
    public createdAt: string;
    public updatedAt: string;

    /**
     * From database
     * @param users
     */
    public fromUsers(users: Users): UsersDTO{
        const userDTO: UsersDTO = new UsersDTO();
        // userDTO.id = users.id;
        if(typeof users.shops !== 'undefined'){
            if(users.shops === null){
                userDTO.shop = null;
            }else {
                userDTO.shop = new ShopsDTO().fromShops(users.shops);
            }
        }
        userDTO.fullname = users.fullname;
        userDTO.username = users.username;
        userDTO.email = users.email;
        // userDTO.password = users.password;
        userDTO.telephone = users.telephone;
        userDTO.address = users.address;
        userDTO.isCourier = users.isCourier;
        userDTO.accessToken = users.accessToken;
        userDTO.refreshToken = users.refreshToken;
        // userDTO.createdAt = users.createdAt;
        // userDTO.updatedAt = users.updatedAt;

        return userDTO;
    }


    /**
     * Email validation
     * @param email
     */
    private emailV(email: string): boolean{
        let regex: RegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email.match(regex)){
            return true;
        }
        return false;
    }

    /**
     * Username validation
     * @param username
     */
    private usernameV(username: string): boolean{
        let regex: RegExp =  /^[a-zA-Z0-9\-]+$/;
        if(username.match(regex)){
            return true;
        }
        return false;
    }

    /**
     * To database
     * @param usersDTO
     * @param response
     */
    public toUsers(usersDTO: UsersDTO, response: Response): Users | ErrorDTO[]{
        const errorDTOs: ErrorDTO[] = [];
        const user: Users = new Users();
        if(usersDTO.shop){
            const shop: Shops | ErrorDTO[] = new ShopsDTO().toShops(usersDTO.shop, response);
            if (response.statusCode !== HttpStatus.OK_STATUS) {
                return shop as ErrorDTO[];
            }
            user.shops = shop as Shops;
        }
        if(!usersDTO.fullname){
            errorDTOs.push(new ErrorDTO().toError('The fullname is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!usersDTO.username){
            errorDTOs.push(new ErrorDTO().toError('The username is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!usersDTO.email){
            errorDTOs.push(new ErrorDTO().toError('The email is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!usersDTO.password){
            errorDTOs.push(new ErrorDTO().toError('The password is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!usersDTO.telephone){
            errorDTOs.push(new ErrorDTO().toError('The telephone number is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!usersDTO.address){
            errorDTOs.push(new ErrorDTO().toError('The address is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!usersDTO.isCourier){
            errorDTOs.push(new ErrorDTO().toError('The courier is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!this.usernameV(usersDTO.username)){
            errorDTOs.push(new ErrorDTO().toError('Incorrect username', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!this.emailV(usersDTO.email)){
            errorDTOs.push(new ErrorDTO().toError('Incorrect email.'+user.password, HttpStatus.BAD_REQUEST_STATUS));
        }
        if(usersDTO.telephone.replace(/\s+/g, '').length !== 10){
            errorDTOs.push(new ErrorDTO().toError('Incorrect telephone number.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(errorDTOs.length){
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }
        user.id = usersDTO.id;
        user.fullname = usersDTO.fullname;
        user.username = usersDTO.username;
        user.email = usersDTO.email;
        user.password = usersDTO.password;
        user.telephone = usersDTO.telephone;
        user.address = usersDTO.address;
        user.isCourier = usersDTO.isCourier;
        user.accessToken = usersDTO.accessToken;
        user.refreshToken = usersDTO.refreshToken;
        user.createdAt = usersDTO.createdAt;
        user.updatedAt = usersDTO.updatedAt;

        return user;
    }
}

