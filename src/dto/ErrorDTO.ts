export class ErrorDTO {

    private message: string;

    private code: number;

    public toError = (message: string, code: number) => {
        const errorDTO: ErrorDTO = new ErrorDTO();
        errorDTO.message = message;
        errorDTO.code = code;

        return errorDTO;
    }
}