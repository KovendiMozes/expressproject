import { Formats } from '../constants/formats';
import { ShopsDTO } from '../dto/ShopsDTO';
import { Shops } from '../entity/Shops';
import { ErrorDTO } from './ErrorDTO';
import { Response } from 'express';
import { HttpStatus } from '../constants/httpStatus';
import { Products } from '../entity/Products';

export class ProductsDTO{
    public id: number;
    public shop: ShopsDTO;
    public name: string;
    public description: string;
    public price: number;
    public createdAt: string;
    public updatedAt: string;

    /**
     * From database
     * @param product
     */
    public fromProducts(product: Products): ProductsDTO{
        const productDTO: ProductsDTO = new ProductsDTO();
        if(typeof product.shops !== 'undefined'){
            if(product.shops === null){
                productDTO.shop = null;
            }else {
                productDTO.shop = new ShopsDTO().fromShops(product.shops);
            }
        }
        productDTO.id = product.id;
        productDTO.name = product.name;
        productDTO.description = product.description;
        productDTO.price = product.price;
        productDTO.createdAt = product.createdAt;
        productDTO.updatedAt = product.updatedAt;

        return productDTO;
    }

    /**
     * Name validation
     * @param name
     * @private
     */
    private nameValidation(name: string): boolean{
        let regex: RegExp =  /^[\s\w]{3,}$/;
        if(name.match(regex)){
            return true;
        }
        return false;
    }

    /**
     * Price validation
     * @param price
     * @private
     */
    private priceValidation(price: string): boolean{
        let regex: RegExp = /^\d{1,}(?:\.\d{0,2}){0,1}$/;
        if(price.match(regex)){
            return true;
        }
        return false;
    }

    /**
     * To database
     * @param productDTO
     * @param response
     */
    public toProducts(productDTO: ProductsDTO, response: Response): Products | ErrorDTO[]{
        const errorDTOs: ErrorDTO[] = [];
        if(!productDTO.shop){
            errorDTOs.push(new ErrorDTO().toError('The shop is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!productDTO.name){
            errorDTOs.push(new ErrorDTO().toError('The name is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!productDTO.description){
            errorDTOs.push(new ErrorDTO().toError('The description is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!productDTO.price){
            errorDTOs.push(new ErrorDTO().toError('The price is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!this.nameValidation(productDTO.name)){
            errorDTOs.push(new ErrorDTO().toError('Incorrect product name.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(typeof productDTO.price !== 'number'){
            errorDTOs.push(new ErrorDTO().toError('Product price not number.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!this.priceValidation(productDTO.price.toString())){
            errorDTOs.push(new ErrorDTO().toError('Incorrect product price.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(errorDTOs.length){
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }
        const product: Products = new Products();
        const shop: Shops | ErrorDTO[] = new ShopsDTO().toShops(productDTO.shop, response);
        if (response.statusCode !== HttpStatus.OK_STATUS) {
            return shop as ErrorDTO[];
        }
        product.shops = shop as Shops;
        product.id = productDTO.id;
        product.name = productDTO.name;
        product.description = productDTO.description;
        product.price = productDTO.price;

        return product;
    }

}