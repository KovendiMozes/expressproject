import { Formats } from '../constants/formats';
import { ErrorDTO } from './ErrorDTO';
import { Response } from 'express';
import { HttpStatus } from '../constants/httpStatus';
import { UsersDTO } from './UsersDTO';
import { Orders } from '../entity/Orders';
import { Users } from '../entity/Users';

export class OrdersDTO{
    public id: number;
    public user: UsersDTO;
    public name: string;
    public orderedDatetime: string;
    public paymentOption: number;
    public status: number;
    public createdAt: string;
    public updatedAt: string;

    /**
     * From database
     * @param order
     */
    public fromOrders(order: Orders): OrdersDTO{
        const orderDTO: OrdersDTO = new OrdersDTO();
        orderDTO.id = order.id;
        if(typeof order.users !== 'undefined'){
            orderDTO.user = new UsersDTO().fromUsers(order.users);
        }
        orderDTO.name = order.name;
        orderDTO.orderedDatetime = order.orderedDatetime;
        orderDTO.paymentOption = order.paymentOption;
        orderDTO.status = order.status;
        orderDTO.createdAt = order.createdAt;
        orderDTO.updatedAt = order.updatedAt;

        return orderDTO;
    }

    /**
     * To database
     * @param orderDTO
     * @param response
     */
    public toOrders(orderDTO: OrdersDTO, response: Response): Orders | ErrorDTO[]{
        const errorDTOs: ErrorDTO[] = [];
        if(!orderDTO.user){
            errorDTOs.push(new ErrorDTO().toError('The user is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(!orderDTO.name){
            errorDTOs.push(new ErrorDTO().toError('The name is required.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(orderDTO.paymentOption !== 0 && orderDTO.paymentOption !== 1){
            errorDTOs.push(new ErrorDTO().toError('Incorrect payment option value.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(errorDTOs.length){
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }
        const order: Orders = new Orders();
        const user: Users | ErrorDTO[] = new UsersDTO().toUsers(orderDTO.user, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return user as ErrorDTO[];
        }
        order.users = user as Users;
        order.name = orderDTO.name;
        order.paymentOption = orderDTO.paymentOption;

        return order;
    }
}