import { ErrorDTO } from '../dto/ErrorDTO';
import { HttpStatus } from '../constants/httpStatus';
import { Response } from 'express';
import moment from 'moment';
import {Formats} from "../constants/formats";
import OrderRepository from '../repository/OrderRepository';
import { Orders } from '../entity/Orders';

class OrderService{
    /**
     * Get all active order
     */
    public getOrders = async (): Promise<Orders[]> => {
        return await OrderRepository.findAll();
    }

    /**
     * Get order by id
     * @param id
     */
    public getOrderById = async (id: string, response: Response): Promise<Orders | ErrorDTO> => {
        const order: Orders = await OrderRepository.findById(parseInt(id));
        if(order){
            return order;
        }
        response.statusCode = HttpStatus.NOT_FOUND_STATUS;
        return new ErrorDTO().toError('The id ' + id + ' order not found in database.', HttpStatus.NOT_FOUND_STATUS);
    }

    /**
     * Get user all own orders
     * @param id
     */
    public getOrdersByUserId = async (id: string): Promise<Orders[]> => {
        return await OrderRepository.findOrdersByUserId(parseInt(id));
    }

    /**
     * Get user all own orders by datetime
     * @param id
     * @param datetime
     */
    public getOrdersByUserIdAndDatetime = async (id: string, datetime: string): Promise<Orders[]> => {
        return await OrderRepository.findOrdersByUserIdAndDatetime(parseInt(id), datetime);
    }

    /**
     * Get active orders by datetime
     * @param datetime
     */
    public getOrdersByDatetime = async (datetime: string): Promise<Orders[]> => {
        return await OrderRepository.findOrdersByDatetime(datetime);
    }

    /**
     * Create order
     * @param order
     * @param response
     */
    public createOrder = async (order: Orders, response: Response): Promise<Orders | ErrorDTO[]> => {
        const errorDTOs: ErrorDTO[] = [];
        //Validation if you need
        if(errorDTOs.length > 0){
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }
        response.statusCode = HttpStatus.CREATED_STATUS;
        return OrderRepository.save(order);
    }

    /**
     * Close the order
     * @param id
     * @param response
     */
    public closeOrder = async (id: string, response: Response): Promise<Orders | ErrorDTO> => {
        const existsOrder: Orders = await OrderRepository.findById(parseInt(id));
        if(existsOrder){
            response.statusCode = HttpStatus.OK_STATUS;
            existsOrder.status = 1;
            return OrderRepository.save(existsOrder);
        }
        response.statusCode = HttpStatus.NOT_FOUND_STATUS;
        return new ErrorDTO().toError('The id ' + id + ' order not found in database.', HttpStatus.NOT_FOUND_STATUS);
    }

    /**
     * Delete order
     * @param id
     * @param response
     */
    public deleteOrder = async (id: string, response: Response): Promise<void | ErrorDTO> => {
        const existsOrder: Orders = await OrderRepository.findById(parseInt(id));
        if(!existsOrder){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            return new ErrorDTO().toError('The id ' + id + ' order not found in database.', HttpStatus.NOT_FOUND_STATUS);
        }
        response.statusCode = HttpStatus.OK_STATUS;
        await OrderRepository.remove(existsOrder);
    }
}

export default OrderService;