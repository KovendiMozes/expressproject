import { ErrorDTO } from '../dto/ErrorDTO';
import { HttpStatus } from '../constants/httpStatus';
import { Response } from 'express';
import moment from 'moment';
import {Formats} from "../constants/formats";
import { SignIn } from '../models/Auth';
import { Users } from '../entity/Users';
import UserRepository from '../repository/UserRepository';
import jwt from 'jsonwebtoken';
import { jwtToken } from '../constants/jwtSecret';

class AuthService {
    public signIn = async (loginRequest: SignIn, response: Response): Promise<Users | ErrorDTO[]> => {
        const errorDTOs: ErrorDTO[] = [];
        const user: Users = await UserRepository.findByUsername(loginRequest.username);
        if(user){
            if(user.password != loginRequest.password){
                errorDTOs.push(new ErrorDTO().toError('Wrong password!', HttpStatus.BAD_REQUEST_STATUS));
            }
            if(errorDTOs.length > 0){
                response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
                return errorDTOs;
            }
            const accesToken: string = jwt.sign({username: user.username, id: user.id}, jwtToken.jwtSecret, { expiresIn: jwtToken.accessToken });
            const refreshToken: string = jwt.sign({username: user.username, id: user.id}, jwtToken.jwtSecret, { expiresIn: jwtToken.refreshToken });

            user.accessToken = accesToken;
            user.refreshToken = refreshToken;
            UserRepository.save(user);

            return user;
        }
        errorDTOs.push(new ErrorDTO().toError('Wrong username!', HttpStatus.NOT_FOUND_STATUS));
        response.statusCode = HttpStatus.NOT_FOUND_STATUS;
        return errorDTOs;
    }

    public accessTokenGenerationInUser = async (id: number, response: Response): Promise<Users | ErrorDTO> => {
        const user: Users = await UserRepository.findById(id);
        if(user){
            user.accessToken = jwt.sign({username: user.username, id: user.id}, jwtToken.jwtSecret, { expiresIn: jwtToken.accessToken });

            UserRepository.save(user);

            return user;
        }
        return new ErrorDTO().toError('User not found in database.', HttpStatus.NOT_FOUND_STATUS);
    }
}

export default AuthService;
