import { ErrorDTO } from '../dto/ErrorDTO';
import { HttpStatus } from '../constants/httpStatus';
import { Response } from 'express';
import moment from 'moment';
import ShopRepository from '../repository/ShopRepository';
import { Shops } from '../entity/Shops';
import { Formats } from '../constants/formats';

class ShopService {
    /**
     * Get all shops
     */
    public getShops = async (): Promise<Shops[]> => {
        return await ShopRepository.findAll();
    }

    /**
     * Get shop by id
     * @param id
     */
    public getShop = async (id: string, response: Response): Promise<Shops | ErrorDTO> => {
        const shop: Shops = await ShopRepository.findById(parseInt(id));
        if(!shop){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            return new ErrorDTO().toError('This shop id: '+id+' not found in database.', HttpStatus.NOT_FOUND_STATUS)
        }
        return shop;
    }

    /**
     * Get shop by name
     * @param name
     */
    public getShopByName = async (name: string, response: Response): Promise<Shops | ErrorDTO> => {
        const shop: Shops = await ShopRepository.findByName(name);
        if(!shop){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            return new ErrorDTO().toError('This shop name: '+name+' not found in database.', HttpStatus.NOT_FOUND_STATUS)
        }
        return shop;
    }

    /**
     * Create shop
     * @param shop
     * @param response
     */
    public createShop = async (shop: Shops, response: Response): Promise<Shops | ErrorDTO[]> => {
        const errorDTOs: ErrorDTO[] = [];
        const verifiedThatShopNameIsUnique: Shops = await ShopRepository.findByName(shop.name);
        if(verifiedThatShopNameIsUnique){
            errorDTOs.push(new ErrorDTO().toError('This shop name: '+shop.name+' already exists.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if (errorDTOs.length > 0) {
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }

        return ShopRepository.save(shop);
    }

    /**
     * Update shop
     * @param shop
     * @param response
     */
    public updateShop = async (shop: Shops, response: Response): Promise<Shops | ErrorDTO[]> =>{
        const existsShop: Shops = await ShopRepository.findById(shop.id);
        const errorDTOs: ErrorDTO[] = [];
        if(existsShop){
            if(shop.name !== existsShop.name){
                const verifiedThatShopNameIsUnique: Shops = await ShopRepository.findByName(shop.name);
                if(verifiedThatShopNameIsUnique){
                    errorDTOs.push(new ErrorDTO().toError('This shop name: '+shop.name+' already exists.', HttpStatus.BAD_REQUEST_STATUS));
                }
            }
            if(errorDTOs.length > 0){
                response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
                return errorDTOs;
            }
            return ShopRepository.save(shop);
        }
        errorDTOs.push(new ErrorDTO().toError(shop.id + 'user not found in database.', HttpStatus.NOT_FOUND_STATUS));
        response.statusCode = HttpStatus.NOT_FOUND_STATUS;
        return errorDTOs;
    }

    /**
     * Delete shop
     * @param id
     * @param response
     */
    public deleteShop = async (id: number, response: Response): Promise<void | ErrorDTO[]> => {
        const existsShop: Shops = await ShopRepository.findById(id);
        const errorDTOs: ErrorDTO[] = [];
        if(!existsShop) {
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            errorDTOs.push(new ErrorDTO().toError('This shop not found in database.', HttpStatus.NOT_FOUND_STATUS));
            return errorDTOs;
        }
        await ShopRepository.remove(existsShop);
    }
}

export default ShopService;