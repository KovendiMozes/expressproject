import UserRepository  from '../repository/UserRepository';
import { ErrorDTO } from '../dto/ErrorDTO';
import { HttpStatus } from '../constants/httpStatus';
import { Response } from 'express';
import moment from 'moment';
import {Users} from "../entity/Users";
import {Formats} from "../constants/formats";

class UserService{

    /**
     * Get all users
     */
    public getUsers = async (): Promise<Users[]> => {
        return await UserRepository.findAll();
    };

    /**
     * Get user by id
     * @param id
     */
    public getUser = async (id: string): Promise<Users> => {
        return await UserRepository.findById(parseInt(id));
    }

    /**
     * Get all courier
     */
    public getCouriers = async (): Promise<Users[]> => {
        return await UserRepository.findCouriers();
    }

    /**
     * Create User
     * @param user
     * @param response
     */
    public createUser = async (user: Users, response: Response): Promise<Users | ErrorDTO[]> => {
        const errorDTOs: ErrorDTO[] = [];
        const verifiedThatUsernameIsUnique: Users = await UserRepository.findByUsername(user.username);
        if(verifiedThatUsernameIsUnique){
            errorDTOs.push(new ErrorDTO().toError('This username: '+user.username+' already exists.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(errorDTOs.length > 0){
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }
        user.accessToken = "";
        user.refreshToken = "";

        return UserRepository.save(user);
    }

    /**
     * Update user
     * @param user
     * @param response
     */
    public updateUser = async (user: Users, response: Response): Promise<Users | ErrorDTO[]> =>{
        const existsUser: Users = await UserRepository.findById(user.id);
        const errorDTOs: ErrorDTO[] = [];
        if(existsUser){
            //validation
            if(errorDTOs.length > 0){
                response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
                return errorDTOs;
            }
            return UserRepository.save(user);
        }
        errorDTOs.push(new ErrorDTO().toError(user.id + 'user not found in database.', HttpStatus.NOT_FOUND_STATUS));
        response.statusCode = HttpStatus.NOT_FOUND_STATUS;
        return errorDTOs;
    }

    /**
     * Delete user
     * @param id
     * @param response
     */
    public deleteUser = async (id: number, response: Response): Promise<void | ErrorDTO[]> => {
        const existsUser: Users = await UserRepository.findById(id);
        const errorDTOs: ErrorDTO[] = [];
        if(!existsUser) {
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            errorDTOs.push(new ErrorDTO().toError('This user not found in database.', HttpStatus.NOT_FOUND_STATUS));
            return errorDTOs;
        }
        await UserRepository.remove(existsUser);
    }

    /**
     * Get user by username
     * @param username
     */
    public getUserByUsername = async (username: string): Promise<Users> => {
        return await UserRepository.findByUsername(username);
    }
}

export default UserService;