import { ErrorDTO } from '../dto/ErrorDTO';
import { HttpStatus } from '../constants/httpStatus';
import { Response } from 'express';
import moment from 'moment';
import {Formats} from "../constants/formats";
import { Products } from '../entity/Products';
import ProductRepository from '../repository/ProductRepository';

class ProductService{
    /**
     * Get all products by shop id
     * @param shopId
     */
    public getProducts = async (shopId: string): Promise<Products[]> => {
        return await ProductRepository.findAll(parseInt(shopId));
    }

    /**
     * Get product by id
     * @param id
     */
    public getProduct = async (id: string): Promise<Products> => {
        return await ProductRepository.findById(parseInt(id));
    }

    /**
     * Get products by name and shop id
     * @param name
     * @param shopId
     */
    public getProductsByNameAndShopId = async (name: string, shopId: string): Promise<Products[]> => {
        return await ProductRepository.findByNameAndShopId(name, parseInt(shopId));
    }

    /**
     * Create product
     * @param product
     * @param response
     */
    public createProduct = async (product: Products, response:Response): Promise<Products | ErrorDTO[]> => {
        const errorDTOs: ErrorDTO[] = [];
        const verifiedThatProductNameIsUniqueThisShop: Products = await ProductRepository.findThatProductNameIsUniqueThisShop(product.name, product.shops.id);
        if(verifiedThatProductNameIsUniqueThisShop){
            errorDTOs.push(new ErrorDTO().toError('This product name: '+product.name+' already exists.', HttpStatus.BAD_REQUEST_STATUS));
        }
        if(errorDTOs.length > 0){
            response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
            return errorDTOs;
        }
        return ProductRepository.save(product);
    }

    /**
     * Update product
     * @param product
     * @param response
     */
    public updateProduct = async (product: Products, response: Response): Promise<Products | ErrorDTO[]> => {
        const errorDTOs: ErrorDTO[] = [];
        const existsProduct: Products = await ProductRepository.findById(product.id);
        if(existsProduct){
           if(existsProduct.name !== product.name){
               const verifiedThatProductNameIsUniqueThisShop: Products = await ProductRepository.findThatProductNameIsUniqueThisShop(product.name, product.shops.id);
               if(verifiedThatProductNameIsUniqueThisShop){
                   errorDTOs.push(new ErrorDTO().toError('This product name: '+product.name+' already exists.', HttpStatus.BAD_REQUEST_STATUS));
               }
           }
            if(errorDTOs.length > 0){
                response.statusCode = HttpStatus.BAD_REQUEST_STATUS;
                return errorDTOs;
            }
            return ProductRepository.save(product);
        }
        errorDTOs.push(new ErrorDTO().toError('The id ' + product.id + ' product not found in database.', HttpStatus.NOT_FOUND_STATUS));
        response.statusCode = HttpStatus.NOT_FOUND_STATUS;
        return errorDTOs;
    }

    /**
     * Delete product
     * @param product
     * @param response
     */
    public deleteProduct = async (id: number, response: Response): Promise<void | ErrorDTO[]> => {
        const errorDTOs: ErrorDTO[] = [];
        const existsProduct: Products = await ProductRepository.findById(id);
        if(!existsProduct){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            errorDTOs.push(new ErrorDTO().toError('This product not found in database.', HttpStatus.NOT_FOUND_STATUS));
            return errorDTOs;
        }
        await ProductRepository.remove(existsProduct);
    }
}

export default ProductService;