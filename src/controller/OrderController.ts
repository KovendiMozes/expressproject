import { Request, Response } from 'express';
import { HttpStatus } from '../constants/httpStatus';
import { ErrorDTO } from '../dto/ErrorDTO';
import jwt from 'jsonwebtoken';
import { jwtToken } from '../constants/jwtSecret';
import UserService from '../service/UserService';
import { Users } from '../entity/Users';
import { Orders } from '../entity/Orders';
import { OrdersDTO } from '../dto/OrdersDTO';
import OrderService from '../service/OrderService';

export class OrderController {
    /**
     * Get all orders
     * @param request
     * @param response
     */
    public static getOrders = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const orders: Orders[] = await new OrderService().getOrders();
        if(!orders.length){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            const errorDTO: ErrorDTO = new ErrorDTO().toError('All orders inactive or empty database.', HttpStatus.NOT_FOUND_STATUS);
            return response.json(errorDTO);
        }
        const ordersDTO: OrdersDTO[] = orders.map((item) => new OrdersDTO().fromOrders(item));
        return response.json(ordersDTO);
    }

    /**
     * Get order by id
     * @param request
     * @param response
     */
    public static getOrder = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const order: Orders | ErrorDTO = await new OrderService().getOrderById(request.params.id, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(order as ErrorDTO);
        }
        const orderDTO: OrdersDTO = new OrdersDTO().fromOrders(order as Orders);
        return response.json(orderDTO);
    }

    /**
     * Create order
     * @param request
     * @param response
     */
    public static createOrder = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(!request.body.user){
            return response.json([new ErrorDTO().toError('The user is required.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        const user: Users = await new UserService().getUser(request.body.user);
        if(!user){
            return response.json([new ErrorDTO().toError('User not found in database.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        request.body.user = user;
        const order: Orders | ErrorDTO[] = new OrdersDTO().toOrders(request.body, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(order as ErrorDTO[])
        }
        const savedOrder: Orders | ErrorDTO[] = await new OrderService().createOrder(order as Orders, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedOrder as ErrorDTO[])
        }
        return response.json(savedOrder as Orders);
    }

    /**
     * Close order
     * @param request
     * @param response
     */
    public static closeOrder = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(!request.params.id){
            return response.json([new ErrorDTO().toError('Order id is required.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        response.statusCode = HttpStatus.OK_STATUS;
        const savedOrder: Orders | ErrorDTO = await new OrderService().closeOrder(request.params.id, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedOrder as ErrorDTO);
        }
        return response.json(savedOrder as Orders);
    }

    /**
     * Delete order
     * @param reqest
     * @param response
     */
    public static deleteOrder = async (reqest: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(!reqest.params.id){
            return response.json([new ErrorDTO().toError('Order id is required.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        const order: void | ErrorDTO = await new OrderService().deleteOrder(reqest.params.id, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(order as ErrorDTO);
        }
        return response.json({ status: 'ok' });
    }
}