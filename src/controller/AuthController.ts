import { Request, Response } from 'express';
import { SignIn } from '../models/Auth';
import { HttpStatus } from '../constants/httpStatus';
import { ErrorDTO } from '../dto/ErrorDTO';
import { ShopsDTO } from '../dto/ShopsDTO';
import AuthService from '../service/AuthService';
import jwt from 'jsonwebtoken';
import { jwtToken } from  '../constants/jwtSecret';
import { Users } from '../entity/Users';
import {Md5} from 'ts-md5/dist/md5';

export class AuthController {
    /**
     * SignIn
     * @param request
     * @param response
     */
    public static signIn = async (request: Request, response: Response) => {
        response.statusCode = HttpStatus.OK_STATUS;
        const loginRequest: SignIn = request.body;
        loginRequest.password  = Md5.hashStr(request.body.password).toString();
        const loginResponse: Users | ErrorDTO[] = await new AuthService().signIn(loginRequest, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(loginResponse);
        }
        return response.json(loginResponse);
    }

    /**
     * New accessToken generation by resfreshToken
     * @param request
     * @param response
     */
    public static newAccessTokenGenerationByRefreshToken = async (request: Request, response: Response) => {
        jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
            if(err) {
                response.json(err);
            }
        })
        const accessTokenGen: Users | ErrorDTO = await new AuthService().accessTokenGenerationInUser(parseInt(request.body.id), response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(accessTokenGen);
        }
        return response.json(accessTokenGen);
    }
}