import { Request, Response } from 'express';
import { Shops } from '../entity/Shops';
import { HttpStatus } from '../constants/httpStatus';
import { ErrorDTO } from '../dto/ErrorDTO';
import { ShopsDTO } from '../dto/ShopsDTO';
import ShopService from '../service/ShopService';
import jwt from 'jsonwebtoken';
import { jwtToken } from '../constants/jwtSecret';

export class ShopController {
    /**
     * Get all shops
     * @param request
     * @param response
     */
    public static getShops = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const shops: Shops[] = await new ShopService().getShops();
        const shopsDTO: ShopsDTO[] = shops.map((item) => new ShopsDTO().fromShops(item));

        response.json(shopsDTO);
    }

    /**
     * Get shop by id
     * @param request
     * @param response
     */
    public static getShop = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const shop: Shops | ErrorDTO = await new ShopService().getShop(request.params.id, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(shop as ErrorDTO);
        }
        const shopDTO: ShopsDTO = new ShopsDTO().fromShops(shop as Shops);

        response.json(shop);
    }

    /**
     * Get shop by shop name
     * @param request
     * @param response
     */
    public static getShopByName = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const shop: Shops | ErrorDTO = await new ShopService().getShopByName(request.params.name, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(shop as ErrorDTO);
        }
        const shopDTO: ShopsDTO = new ShopsDTO().fromShops(shop as Shops);

        response.json(shop);
    }

    /**
     * Create shop
     * @param request
     * @param response
     */
    public static createShop = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const shop: Shops | ErrorDTO[] = new ShopsDTO().toShops(request.body, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(shop as ErrorDTO[]);
        }
        const savedShops: Shops | ErrorDTO[] = await new ShopService().createShop(shop as Shops, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedShops as ErrorDTO[]);
        }

        return response.json(savedShops as Shops);
    }

    /**
     * Update shop
     * @param request
     * @param response
     */
    public static updateShop = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(request.body.id !== parseInt(request.params.id)){
            return response.json([new ErrorDTO().toError('The body id and params id is not match.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        response.statusCode = HttpStatus.OK_STATUS;
        const shop: Shops | ErrorDTO[] = new ShopsDTO().toShops(request.body, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(shop as ErrorDTO[]);
        }
        const savedShop: Shops | ErrorDTO[] = await new ShopService().updateShop(shop as Shops, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedShop as ErrorDTO[]);
        }

        return response.json(savedShop as Shops);
    }

    /**
     * Delete shop
     * @param request
     * @param response
     */
    public static deleteShop = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(!request.params.id){
            return response.json([new ErrorDTO().toError('The parameter shop id is missing.', HttpStatus.NOT_FOUND_STATUS)]);
        }
        const shop: void | ErrorDTO[] = await new ShopService().deleteShop(parseInt(request.params.id), response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(shop);
        }
        response.statusCode = HttpStatus.OK_STATUS;
        return response.json({ status: 'ok' });
    }


}