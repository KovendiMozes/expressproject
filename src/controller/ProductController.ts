import { Request, Response } from 'express';
import { Shops } from '../entity/Shops';
import ShopService from '../service/ShopService';
import { HttpStatus } from '../constants/httpStatus';
import { ErrorDTO } from '../dto/ErrorDTO';
import { Products } from '../entity/Products';
import { ProductsDTO } from '../dto/ProductsDTO';
import ProductService from '../service/ProductService';
import jwt from 'jsonwebtoken';
import { jwtToken } from '../constants/jwtSecret';

export class ProductController{
    /**
     * Get all product by shop id
     * @param request
     * @param response
     */
    public static getProducts = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const products: Products[] = await new ProductService().getProducts(request.params.id);
        if(!products.length){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            return response.json(new ErrorDTO().toError('This shop '+ request.params.id +' has no products in database.', HttpStatus.NOT_FOUND_STATUS));
        }
        const productsDTO: ProductsDTO[] = products.map((item) => new ProductsDTO().fromProducts(item));

        response.json(productsDTO);
    }

    /**
     * Get product by id
     * @param request
     * @param response
     */
    public static getProduct = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const product: Products = await new ProductService().getProduct(request.params.id);
        if(!product){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            return response.json(new ErrorDTO().toError('This product '+ request.params.id +' not found in database.', HttpStatus.NOT_FOUND_STATUS));
        }
        const productDTO: ProductsDTO = new ProductsDTO().fromProducts(product);

        response.json(productDTO);
    }

    /**
     * Get Products by shop id and product name
     * @param request
     * @param response
     */
    public static getProductsByNameAndShopId = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const products: Products[] = await new ProductService().getProductsByNameAndShopId(request.params.name, request.params.id);
        if(!products.length){
            response.statusCode = HttpStatus.NOT_FOUND_STATUS;
            return response.json(new ErrorDTO().toError('This shop '+ request.params.id +' has no '+ request.params.name +' product in database.', HttpStatus.NOT_FOUND_STATUS));
        }
        const productsDTO: ProductsDTO[] = products.map((item) => new ProductsDTO().fromProducts(item));

        response.json(productsDTO);
    }

    /**
     * Create product
     * @param request
     * @param response
     */
    public static createProduct = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(!request.body.shop){
            return response.json([new ErrorDTO().toError('The shop is required.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        const shop: Shops | ErrorDTO =  await new ShopService().getShop(request.body.shop, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(shop as ErrorDTO);
        }
        request.body.shop = shop;
        const product: Products | ErrorDTO[] = new ProductsDTO().toProducts(request.body, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(product as ErrorDTO[]);
        }
        const savedProduct: Products | ErrorDTO[] = await new ProductService().createProduct(product as Products, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedProduct as ErrorDTO[]);
        }
        return response.json(savedProduct as Products)
    }

    /**
     * Update product
     * @param request
     * @param response
     */
    public static updateProduct = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(request.body.id !== parseInt(request.params.id)){
            return response.json([new ErrorDTO().toError('The body id and params id is not match.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        if(request.body.shop){
            const shop: Shops | ErrorDTO =  await new ShopService().getShop(request.body.shop, response);
            if(response.statusCode !== HttpStatus.OK_STATUS){
                return response.json(shop as ErrorDTO);
            }
            request.body.shop = shop;
        }
        response.statusCode = HttpStatus.OK_STATUS;
        const product: Products | ErrorDTO[] = new ProductsDTO().toProducts(request.body, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(product as ErrorDTO[]);
        }
        const savedProduct: Products | ErrorDTO[] = await new ProductService().updateProduct(product as Products, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedProduct as ErrorDTO[]);
        }

        return response.json(savedProduct as Products);
    }

    /**
     * Delete product
     * @param request
     * @param response
     */
    public static deleteProduct = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(!request.params.id){
            return response.json([new ErrorDTO().toError('The parameter user id is missing.', HttpStatus.NOT_FOUND_STATUS)]);
        }
        const product: void | ErrorDTO[] = await new ProductService().deleteProduct(parseInt(request.params.id), response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(product as ErrorDTO[]);
        }
        response.statusCode = HttpStatus.OK_STATUS;
        return response.json({ status: 'ok' });
    }

}