import { Request, Response } from 'express';
import { HttpStatus } from '../constants/httpStatus';
import { ErrorDTO } from '../dto/ErrorDTO';
import { Users } from '../entity/Users';
import UserService from '../service/UserService';
import {UsersDTO} from "../dto/UsersDTO";
import jwt from 'jsonwebtoken';
import { jwtToken } from  '../constants/jwtSecret';
import { Shops } from '../entity/Shops';
import ShopService from '../service/ShopService';
import {Md5} from 'ts-md5/dist/md5';

export class UserController {
    /**
     * Get all shoppings
     * @param request
     * @param response
     */
    public static getUsers = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const users: Users[] = await new UserService().getUsers();
        const usersDTO: UsersDTO[] = users.map((item) => new UsersDTO().fromUsers(item));

        response.json(usersDTO);
    }

    /**
     * Get user by id
     * @param request
     * @param response
     */
    public static getUser = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const user = await new UserService().getUser(request.params.id);
        const userDTO: UsersDTO = new UsersDTO().fromUsers(user);

        response.json(userDTO);
    }

    /**
     * Get all couriers in users
     * @param request
     * @param response
     */
    public static getCouriersInUsers = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        response.statusCode = HttpStatus.OK_STATUS;
        const couriers: Users[] = await new UserService().getCouriers();
        const couriersDTO: UsersDTO[] = couriers.map((item) => new UsersDTO().fromUsers(item));

        response.json(couriersDTO);
    }

    /**
     * Create user
     * @param request
     * @param response
     */
    public static createUser = async (request: Request, response: Response) => {
        if(request.body.shop){
            const shop: Shops | ErrorDTO =  await new ShopService().getShop(request.body.shop, response);
            if(response.statusCode !== HttpStatus.OK_STATUS){
                return response.json(shop as ErrorDTO);
            }
            request.body.shop = shop;
        }
        request.body.password  = Md5.hashStr(request.body.password).toString();
        const user: Users | ErrorDTO[] = new UsersDTO().toUsers(request.body, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(user as ErrorDTO[]);
        }
        const savedUser: Users | ErrorDTO[] = await new UserService().createUser(user as Users, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedUser);
        }

        return response.json(savedUser);
    }

    /**
     * Update user
     * @param request
     * @param response
     */
    public static updateUser = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(request.body.id !== parseInt(request.params.id)){
            return response.json([new ErrorDTO().toError('The body id and params id is not match.', HttpStatus.BAD_REQUEST_STATUS)]);
        }
        if(request.body.shop){
            const shop: Shops | ErrorDTO =  await new ShopService().getShop(request.body.shop, response);
            if(response.statusCode !== HttpStatus.OK_STATUS){
                return response.json(shop as ErrorDTO);
            }
            request.body.shop = shop;
        }
        response.statusCode = HttpStatus.OK_STATUS;
        const user: Users | ErrorDTO[] = new UsersDTO().toUsers(request.body, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(user as ErrorDTO[]);
        }
        const savedUser: Users | ErrorDTO[] = await new UserService().updateUser(user as Users, response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(savedUser);
        }

        return response.json(savedUser);
    }

    /**
     * Delete user
     * @param request
     * @param response
     */
    public static deleteUser = async (request: Request, response: Response) => {
        // jwt.verify(request.headers.authorization, jwtToken.jwtSecret, (err: any) => {
        //     if(err){
        //         response.json(err);
        //     }
        // })
        if(!request.params.id){
            return response.json([new ErrorDTO().toError('The parameter user id is missing.', HttpStatus.NOT_FOUND_STATUS)]);
        }
        const user: void | ErrorDTO[] = await new UserService().deleteUser(parseInt(request.params.id), response);
        if(response.statusCode !== HttpStatus.OK_STATUS){
            return response.json(user);
        }
        response.statusCode = HttpStatus.OK_STATUS;
        return response.json({ status: 'ok' });
    }
}
