import { UserController } from "../controller/UserController";
import { ShopController } from "../controller/ShopController";
import { AuthController } from "../controller/AuthController";
import { ProductController } from "../controller/ProductController";
import { OrderController } from "../controller/OrderController";

module.exports = (route: any) => {

    /**
     * Get all users
     */
    route.get(`${process.env.API_URL}/users`, UserController.getUsers);

    /**
     * Get user by id
     */
    route.get(`${process.env.API_URL}/user/:id`, UserController.getUser);

    /**
     * Get all couriers in users
     */
    route.get(`${process.env.API_URL}/couriers`, UserController.getCouriersInUsers);

    /**
     * Create user
     */
    route.post(`${process.env.API_URL}/signup`, UserController.createUser);

    /**
     * Update user
     */
    route.put(`${process.env.API_URL}/user/:id`, UserController.updateUser);

    /**
     * Delete user
     */
    route.delete(`${process.env.API_URL}/user/:id`, UserController.deleteUser);

    /**
     * Create shop
     */
    route.post(`${process.env.API_URL}/shop`, ShopController.createShop);

    /**
     * Get all shops
     */
    route.get(`${process.env.API_URL}/shops`, ShopController.getShops);

    /**
     * Get shop by id
     */
    route.get(`${process.env.API_URL}/shop/:id`, ShopController.getShop);

    /**
     * Get shop by name
     */
    route.get(`${process.env.API_URL}/shop/name/:name`, ShopController.getShopByName);

    /**
     * Update shop
     */
    route.put(`${process.env.API_URL}/shop/:id`, ShopController.updateShop);

    /**
     * Delete shop
     */
    route.delete(`${process.env.API_URL}/shop/:id`, ShopController.deleteShop);

    /**
     * SignIn
     */
    route.post(`${process.env.API_URL}/signin`, AuthController.signIn);

    /**
     * New accessToken generation by refreshToken
     */
    route.post(`${process.env.API_URL}/new_access_token`, AuthController.newAccessTokenGenerationByRefreshToken);

    /**
     * Create product
     */
    route.post(`${process.env.API_URL}/product`, ProductController.createProduct);

    /**
     * Get products by shop id
     */
    route.get(`${process.env.API_URL}/products/:id`, ProductController.getProducts);

    /**
     * Get product by id
     */
    route.get(`${process.env.API_URL}/product/:id`, ProductController.getProduct);

    /**
     * Get Products by shop id and product name
     */
    route.get(`${process.env.API_URL}/products/:id/:name`, ProductController.getProductsByNameAndShopId);

    /**
     * Update product
     */
    route.put(`${process.env.API_URL}/product/:id`, ProductController.updateProduct);

    /**
     * Delete product
     */
    route.delete(`${process.env.API_URL}/product/:id`, ProductController.deleteProduct);

    /**
     * Create order
     */
    route.post(`${process.env.API_URL}/order`, OrderController.createOrder);

    /**
     * Get all orders
     */
    route.get(`${process.env.API_URL}/orders`, OrderController.getOrders);

    /**
     * Get order by id
     */
    route.get(`${process.env.API_URL}/order/:id`, OrderController.getOrder);

    /**
     * Close/Update order
     */
    route.put(`${process.env.API_URL}/order/:id`, OrderController.closeOrder);

    /**
     * Delete order
     */
    route.delete(`${process.env.API_URL}/order/:id`, OrderController.deleteOrder);
};
