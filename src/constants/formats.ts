export enum Formats {
    MYSQL_DATE = 'YYYY-MM-DD',
    MYSQL_DATE_TIME = 'YYYY-MM-DD HH:mm:ss',
}