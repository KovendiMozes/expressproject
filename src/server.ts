import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import { createConnection } from 'typeorm';

createConnection()
    .then(async () => {
        dotenv.config();
        const server = express();
        server.use(cors());
        server.use(express.static('public'));
        server.use(bodyParser.urlencoded({ extended: false }));
        server.use(bodyParser.json());
        require('./router/Router')(server);
        server.listen(process.env.PORT, () => console.log('The server run on ' + process.env.PORT));
    })
    .catch((error) => 'Db connection error:' + error);
